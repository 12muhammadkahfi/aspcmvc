﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using MVCWithoutEF.Models;
namespace MVCWithoutEF.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            List<CustomerModel> cModel = new List<CustomerModel>();
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            string query = "SELECT * FROM customers";
            using (SqlConnection con = new SqlConnection(query))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            cModel.Add(new CustomerModel
                            {
                                CustomerID = Convert.ToInt32(sdr["CustomerID"]),
                                Name = Convert.ToString(sdr["Name"]),
                                Country = Convert.ToString(sdr["Country"])
                            });
                        }
                    }
                    con.Close();
                }
            }
            //Inline Checking Existing Data
            if (cModel.Count == 0)
            {
                cModel.Add(new CustomerModel());
            }
            return View(cModel);
        }

        //Inline Add/Insert Data
        [HttpPost]
        public JsonResult InsertCustomer(CustomerModel cModel)
        {
            string query = "INSERT INTO CUSTOMER VALUES(@Name, @Country)";
            query += "SELECT SCOPE_IDENTITY*()";
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Parameters.AddWithValue("@Name", cModel.Name);
                    cmd.Parameters.AddWithValue("@Country", cModel.Country);
                    cmd.Connection = con;
                    con.Open();
                    cModel.CustomerID = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
            }
            //        return JsonResult(cModel);
            return Json(cModel);
        }
        [HttpPost]
        public ActionResult UpdateCutomers(CustomerModel cModel)
        {
            string query = "UPDATE CUSTOMER SET  Name=@Name, Country=@Country WHERE CustomerID=@CustomerID";
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Parameters.AddWithValue("@CustomerID", cModel.CustomerID);
                    cmd.Parameters.AddWithValue("@Name", cModel.Name);
                    cmd.Parameters.AddWithValue("@Country", cModel.Country);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult DeleteCustomers(int CusId)
        {
            string query = "DELETE FROM CUSTOMER WHERE CustomerID=@CustomerID";
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Parameters.AddWithValue("@CustomerID", CusId);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                }
            }
            return new EmptyResult();
        }
    }
}
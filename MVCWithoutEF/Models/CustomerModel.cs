﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCWithoutEF.Models
{
    public class CustomerModel
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
    }
}